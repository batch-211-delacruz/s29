//part1


db.users.find(
{$or: 
    [
        {
               firstName: {$regex: 's', $options: '$i'}
        }, 
        {
               lastName: {$regex: 'd', $options: '$i'}
        }
    ]
},
{
    firstName: 1,
    lastName: 1,
    _id: 0 
}
);


//part2


db.users.find(
    {$and: 
        [
            {
                 department: {$regex: 'hr', $options: '$i'}
            }, 
            {
                 age: {$gte: 70}
            }
        ]
    }
);


//part 3


db.users.find(
    {$and: 
        [
            {
                firstName: {$regex: 'e', $options: '$i'}
            }, 
            {
                age: {$lte: 30}
            }
        ]
    }
);